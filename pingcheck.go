package pingcheck

import (
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

type respon struct {
	Ms          string `json:"ms"`
	Status      int16  `json:"status"`
	Message     string `json:"message"`
	IpUtimeExec string `json:"ip_uptime_exec"`
}

func pingCheck(req string) respon {
	var res respon
	out, _ := exec.Command("ping", req, "-c 1", "-W 10").Output()
	res.IpUtimeExec = "103.160.37.141"
	fmt.Println(string(out))
	if strings.Contains(string(out), "Name or service not known") || strings.Contains(string(out), "Request time out") || strings.Contains(string(out), "100.0% packet loss") {
		res.Ms = "0"
		res.Message = "not ok"
		res.Status = 0
	} else {

		latencyPattern := regexp.MustCompile(`(round-trip|rtt)\s+\S+\s*=\s*([0-9.]+)/([0-9.]+)/([0-9.]+)/([0-9.]+)\s*ms`)
		matches := latencyPattern.FindAllStringSubmatch(string(out), -1)
		for _, item := range matches {
			res.Ms = strings.TrimSpace(item[3])
			res.Status = 1
			res.Message = "ping ok"

		}
	}
	return res
}
